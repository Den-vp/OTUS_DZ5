﻿using System;
using System.Text;
using System.Reflection;
using System.IO;

namespace ReflectionDZ5
{
    class CsvSerializer : ISerializer
    {
        private static BindingFlags flags => BindingFlags.Instance | BindingFlags.Public;

        public void Serialize<TmyObj>(TmyObj obj, FileStream file)
        {
            var properties = obj.GetType().GetProperties(flags);
            foreach (var field in properties)
            {
                byte[] array = System.Text.Encoding.Default.GetBytes($"{field.Name}:{field.GetValue(obj)};");
                file.Write(array, 0, array.Length);
            }
        }

        public TmyObj Deserialize<TmyObj>(FileStream data) where TmyObj : new()
        {
            byte[] array = new byte[data.Length];
            data.Read(array, 0, array.Length);
            string strData = System.Text.Encoding.Default.GetString(array);

            TmyObj obj = (TmyObj)typeof(TmyObj).GetConstructor(Type.EmptyTypes).Invoke(new object[0]);
            var properties = obj.GetType().GetProperties(flags);
            var values = strData.Split(';', StringSplitOptions.RemoveEmptyEntries);
            if (properties.Length != values.Length)
                throw new FormatException("Количество переданных свойств не совпадает с объектом!");

            foreach (var value in values)
            {
                var arr = value.Split(':');
                var field = obj.GetType().GetProperty(arr[0], flags);
                if (field != null)
                {
                    field.SetValue(obj, Convert.ChangeType(arr[1], field.PropertyType));
                }
            }
            return obj;
        }
    }
}