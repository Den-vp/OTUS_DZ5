﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ReflectionDZ5
{
    public class XmlSerial : ISerializer
    {   
        public void Serialize<TmyObj>(TmyObj obj, FileStream file)
        {
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            serializer.Serialize(file, obj);
        }
        public TmyObj Deserialize<TmyObj>(FileStream data) where TmyObj : new()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TmyObj));
            TmyObj obj = (TmyObj)typeof(TmyObj).GetConstructor(Type.EmptyTypes).Invoke(new object[0]);
            obj = (TmyObj)serializer.Deserialize(data);
            return obj;
        }
    }
}