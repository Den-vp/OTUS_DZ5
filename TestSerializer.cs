﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace ReflectionDZ5
{
    class TestSerializer
    {
        private readonly int iterations;
        private readonly ISerializer serializer;
        private readonly string expansion;

        public TestSerializer(int iterations, ISerializer serializer, string expansion)
        {
            this.iterations = iterations;
            this.serializer = serializer;
            this.expansion = expansion;
        }

        public string TestRunSerializing<TmyObj>(TmyObj obj) where TmyObj : new()
        {
            string outinfo = $"Кол-во итераций сериализации: {iterations}\r\n";
            Stopwatch stopwatch = new Stopwatch();
            string path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string fileName = path+@"\file\" + serializer.GetType().Name + expansion;

            try
            {
                stopwatch.Start();
                for (int i = 0; i < iterations; i++)
                {
                    try
                    {
                        using (FileStream stream = new FileStream(fileName, FileMode.OpenOrCreate))
                        {
                            serializer.Serialize(obj, stream);
                            stream.Close();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Ошибка {e.ToString()}; i= {i}");
                    }
                }
                stopwatch.Stop();
                string strProperties = string.Empty;
                using (FileStream fs = File.OpenRead(fileName))
                {
                    byte[] array = new byte[fs.Length];
                    fs.Read(array, 0, array.Length);
                    strProperties = System.Text.Encoding.Default.GetString(array);
                }

                outinfo += $"Сериализатор: {serializer.GetType().Name}{Environment.NewLine} Строка свойств класса: {strProperties}\r\n";
                outinfo += $"{serializer.GetType().Name}.{serializer.GetType().GetMethods().FirstOrDefault().Name}:\t{stopwatch.Elapsed.Minutes}мин {stopwatch.Elapsed.Seconds}сек.{stopwatch.Elapsed.Milliseconds}мсек.\r\n";

                stopwatch.Start();
                for (int i = 0; i < iterations; i++)
                {
                    using (FileStream streamread = File.OpenRead(fileName))
                    {
                        TmyObj objF = serializer.Deserialize<TmyObj>(streamread);
                    }
                }
                stopwatch.Stop();

                outinfo += $"{serializer.GetType().Name}.{serializer.GetType().GetMethods().ElementAt(1).Name}:\t{stopwatch.Elapsed.Minutes}мин {stopwatch.Elapsed.Seconds}сек.{stopwatch.Elapsed.Milliseconds}мсек.{Environment.NewLine}";
            }
            catch (Exception e)
            {
                throw new FormatException($"Неверный формат данных! \r\n{e.Message}");
            }
            return outinfo;
        }
    }
}